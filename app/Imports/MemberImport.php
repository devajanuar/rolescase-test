<?php

namespace App\Imports;

use App\Models\Group;
use App\Models\Member;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Maatwebsite\Excel\Concerns\ToModel;

class MemberImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
		$group = Group::where('namagroup', $row[1])->first();

		if($group != null){
			$groupid = $group->id;
		}else{
			$data = Group::create([
				'namagroup' => $row[1],
				'kota' => $row[1]
			]);
			$groupid = $data->id;
		}

		$id = IdGenerator::generate(['table' => 'members', 'field' => 'member_id', 'length' => 7, 'prefix' => 'M-']);

        return new Member([
            'member_id' => $id,
			'groupid' => $groupid,
			'nama' => $row[2],
			'alamat' => $row[3],
			'hp' => $row[4],
			'email' => $row[5],
        ]);
    }
}
