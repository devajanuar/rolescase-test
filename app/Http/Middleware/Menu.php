<?php

namespace App\Http\Middleware;

use App\Models\RoleMenu;
use App\Models\UserMenu;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Menu
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
	 * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
	 */
	public function handle(Request $request, Closure $next)
	{
		$path = $request->path();
		$menu_id = RoleMenu::where('url', $path)->pluck('id')->first();

		if ($menu_id) {
			$user_menu = UserMenu::where('user_id', Auth::user()->id)
				->where('role_menu', $menu_id)
				->count();

			if ($user_menu == 0) {
				return redirect()->back()->with('error', 'Anda tidak bisa mengakses menu ini !');
			}
		}
		return $next($request);
	}
}
