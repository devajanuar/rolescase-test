<?php

namespace App\Http\Controllers;

use App\Imports\MemberImport;
use App\Models\Group;
use App\Models\Member;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class MasterController extends Controller
{
	public function index_group()
	{
		try {
			$data = Group::all();

			return view('pages.group', ['group' => $data]);
		} catch (\Throwable $th) {
			return redirect()->back()->with('error', $th->getMessage());
		}
	}

	public function add_group(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required|string',
			'kota' => 'required|string',
		]);

		if ($validator->fails()) {
			return redirect()->back()->with('error', $validator->errors()->first());
		}

		$name = $request->input('name');
		$kota = $request->input('kota');

		DB::beginTransaction();
		try {
			Group::create([
				'namagroup' => $name,
				'kota' => $kota
			]);

			DB::commit();
			return redirect()->back()->with('status', 'Sukses menambah group');
		} catch (\Throwable $th) {
			DB::rollBack();
			return redirect()->back()->with('error', $th->getMessage());
		}
	}

	public function update_group(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'up-id' => 'required|integer',
			'up-name' => 'required|string',
			'up-kota' => 'required|string',
		]);

		if ($validator->fails()) {
			return redirect()->back()->with('error', $validator->errors()->first());
		}

		$id = $request->input('up-id');
		$name = $request->input('up-name');
		$kota = $request->input('up-kota');

		DB::beginTransaction();
		try {
			Group::where('id', $id)->update([
				'namagroup' => $name,
				'kota' => $kota
			]);

			DB::commit();
			return redirect()->back()->with('status', 'Update data group berhasil');
		} catch (\Throwable $th) {
			DB::rollBack();
			return redirect()->back()->with('error', $th->getMessage());
		}
	}

	public function destroy_group(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'id' => 'required|integer',
		]);

		if ($validator->fails()) {
			return redirect()->back()->with('error', $validator->errors()->first());
		}

		$id = $request->input('id');

		$cek_member = Member::where('groupid', $id)->count();

		if ($cek_member > 0) {
			return redirect()->back()->with('error', 'Data group telah digunakan member, tidak dapat menghapus');
		}

		DB::beginTransaction();

		try {
			Group::where('id', $id)->delete();

			DB::commit();
			return redirect()->back()->with('status', 'Data group berhasil di hapus');
		} catch (\Throwable $th) {
			DB::rollBack();
			return redirect()->back()->with('error', $th->getMessage());
		}
	}

	public function index_member(Request $request)
	{
		$limit = $request->input('limit') ?? 25;
		try {
			$data = Member::with('group')->orderByDesc('created_at')->paginate($limit);
			$group = Group::all();

			return view('pages.member', ['member' => $data, 'group' => $group]);
		} catch (\Throwable $th) {
			return redirect()->back()->with('error', $th->getMessage());
		}
	}

	public function add_member(Request $request)
	{
		$folder = 'member';

		$validator = Validator::make($request->all(), [
			'groupid' => 'required|integer',
			'name' => 'required|string',
			'alamat' => 'required|string',
			'hp' => 'required|string',
			'email' => 'required|email',
			'picture' => 'required|mimes:jpg,png,gif|max:2048'
		]);

		if ($validator->fails()) {
			return redirect()->back()->with('error', $validator->errors()->first());
		}

		$name = $request->input('name');
		$groupid = $request->input('groupid');
		$alamat = $request->input('alamat');
		$hp = $request->input('hp');
		$email = $request->input('email');
		$id = IdGenerator::generate(['table' => 'members', 'field' => 'member_id', 'length' => 7, 'prefix' => 'M-']);

		if ($request->file('picture') != null) {
			if ($request->file('picture')->getSize() > 2097152) {
				return redirect()->back()->with('error', 'Ukuran maksimal foto adalah 2Mb');
			}
			$picture = $this->upload_file($request->file('picture'), $folder);
		} else {
			$picture = null;
		}

		DB::beginTransaction();
		try {
			Member::create([
				'member_id' => $id,
				'groupid' => $groupid,
				'nama' => $name,
				'alamat' => $alamat,
				'hp' => $hp,
				'email' => $email,
				'profile_pic' => $picture
			]);

			DB::commit();
			return redirect()->back()->with('status', 'Sukses menambah Member');
		} catch (\Throwable $th) {
			DB::rollBack();
			return redirect()->back()->with('error', $th->getMessage());
		}
	}

	public function update_member(Request $request)
	{
		$folder = 'member';

		$validator = Validator::make($request->all(), [
			'up-id' => 'required|string',
			'up-name' => 'required|string',
			'up-groupid' => 'required|integer',
			'up-alamat' => 'required|string',
			'up-hp' => 'required|string',
			'up-email' => 'required|email',
		]);

		if ($validator->fails()) {
			return redirect()->back()->with('error', $validator->errors()->first());
		}

		$id = $request->input('up-id');
		$name = $request->input('up-name');
		$groupid = $request->input('up-groupid');
		$alamat = $request->input('up-alamat');
		$hp = $request->input('up-hp');
		$email = $request->input('up-email');

		if ($request->file('up-picture') != null) {
			if ($request->file('up-picture')->getSize() > 2048) {
				return redirect()->back()->with('error', 'Ukuran maksimal foto adalah 2Mb');
			}
			$picture = $this->upload_file($request->file('up-picture'), $folder);
			$req = [
				'groupid' => $groupid,
				'nama' => $name,
				'alamat' => $alamat,
				'hp' => $hp,
				'email' => $email,
				'profile_pic' => $picture
			];
		} else {
			$req = [
				'groupid' => $groupid,
				'nama' => $name,
				'alamat' => $alamat,
				'hp' => $hp,
				'email' => $email
			];
		}

		DB::beginTransaction();
		try {
			Member::where('member_id', $id)->update($req);

			DB::commit();
			return redirect()->back()->with('status', 'Update data member berhasil');
		} catch (\Throwable $th) {
			DB::rollBack();
			return redirect()->back()->with('error', $th->getMessage());
		}
	}

	public function destroy_member(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'id' => 'required|integer',
		]);

		if ($validator->fails()) {
			return redirect()->back()->with('error', $validator->errors()->first());
		}

		$id = $request->input('id');

		DB::beginTransaction();

		try {
			Member::where('id', $id)->delete();

			DB::commit();
			return redirect()->back()->with('status', 'Data group berhasil di hapus');
		} catch (\Throwable $th) {
			DB::rollBack();
			return redirect()->back()->with('error', $th->getMessage());
		}
	}

	public function import_member(Request $request)
	{
		DB::beginTransaction();
		try {
			Excel::import(new MemberImport, $request->file('file')->store('temp'));
			
			DB::commit();
			return redirect()->back()->with('status', 'Import data member berhasil');
		} catch (\Throwable $th) {
			DB::rollBack();
			return redirect()->back()->with('error', $th->getMessage());
		}
	}
}
