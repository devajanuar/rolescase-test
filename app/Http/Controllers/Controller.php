<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public static function success($data = null, $message = null)
	{
		$res = [
			'status' => 200,
			'message' => $message,
			'data' => $data
		];

		return response()->json($res, $res['status']);
	}

	public static function error($message = null)
	{
		$res = [
			'status' => 400,
			'message' => $message,
			'data' => null
		];

		return response()->json($res, $res['status']);
	}

	public function upload_file($file, $path)
	{
		if ($file != null) {
			$ext = $file->getClientOriginalExtension();
			$filename = Str::random(15) . '.' . $ext;
			$destinationPath = 'upload/' . $path . '/';

			if (!is_dir(public_path($destinationPath))) {
				File::makeDirectory(public_path($destinationPath), 0777);
			}

			if ($file->move(public_path($destinationPath), $filename)) {
				return url($destinationPath) . '/' . $filename;
			} else {
				return null;
			}
		}
	}
}
