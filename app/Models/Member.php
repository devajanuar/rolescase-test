<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;

	protected $table = 'members';
	protected $keyType = 'string';

	protected $fillable = ['member_id', 'nama', 'groupid', 'alamat', 'hp', 'email', 'profile_pic'];

	public function group()
	{
		return $this->hasOne(Group::class, 'id', 'groupid');
	}
}
