<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMenu extends Model
{
    use HasFactory;

	protected $tables = 'user_menus';
	protected $fillable = ['user_id', 'role_menu'];

	public function menu()
	{
		return $this->hasOne(RoleMenu::class, 'id', 'role_menu');
	}
}
