@extends('layouts.app')

@section('content')
<div class="container-fluid mt-3">
	<div class="row mb-5">
		<div class="col">
			<h1>Group Management</h1>
		</div>
	</div>
	<!-- <form class="row" action="{{ url('/user-search') }}" method="GET">
		<div class="col">
			<div class="form-group">
				<div class="input-group mb-4">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fa fa-search"></i></span>
					</div>
					<input class="form-control" value="{{isset($keyword) ? $keyword : ''}}" placeholder="Search" name="keyword" type="text">
				</div>
			</div>
		</div>
		<div class="mr-2">
			@if (isset($filter))
			<a href="{{url('/user')}}" class="btn btn-danger">Reset Filter</a>
			@endif
			<button type="submit" data-toggle="dropdown" class="btn btn-success">Filter</button>
			<div class="dropdown-menu dropdown-menu-right mt-2" aria-labelledby="print-toggle">
				<a class="dropdown-item" href="{{url('/user')}}">Semua User</a>
				<a class="dropdown-item" href="{{url('/user?filter=2')}}">Desa</a>
				<a class="dropdown-item" href="{{url('/user?filter=3')}}">Kecamatan</a>
				<a class="dropdown-item" href="{{url('/user?filter=4')}}">Staff</a>
				<a class="dropdown-item" href="{{url('/user?filter=5')}}">Kasi</a>
				<a class="dropdown-item" href="{{url('/user?filter=6')}}">Kabid</a>
			</div>
		</div>
		<div class="mr-3">
			<button type="submit" class="btn btn-primary">Search</button>
		</div>
	</form> -->
	@if (session()->has('status'))
	<div class="alert alert-success alert-dismissible fade show" role="alert">
		<span class="alert-inner--text"><strong>Sukses! </strong> {{ session()->get('status') }}</span>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	@endif
	@if (session()->has('error'))
	<div class="alert alert-danger alert-dismissible fade show" role="alert">
		<span class="alert-inner--text"><strong>Error! </strong> {{ session()->get('error') }}</span>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	@endif

	<div class="row">
		<div class="col">
			<div class="card">
				<!-- Card header -->
				<div class="card-header border-0">
					<div class="row">
						<div class="col form-inline">
							<label>
								Show
								<form action="{{route('group')}}" method="GET">
									<select onchange="this.form.submit()" class="form-custom-select" id="limit" name="limit">
										@if (isset($limit))
										<option value="{{$limit}}" disabled selected>{{$limit}}</option>
										@endif
										<option value="5">5</option>
										<option value="10">10</option>
										<option value="25">25</option>
										<option value="50">50</option>
										<option value="100">100</option>
									</select>
								</form>
								Entries
							</label>
						</div>
						<div class="col-4 text-right">
							<a href="#" data-toggle="modal" data-target="#modal-default" class="btn btn-sm btn-primary">Add Group</a>
						</div>
					</div>
				</div>
				<!-- Light table -->
				<div class="table-responsive">
					<table class="table align-items-center table-flush">
						<thead class="thead-light">
							<tr>
								<th scope="col" class="sort" data-sort="name">ID</th>
								<th scope="col" class="sort" data-sort="status">Nama Group</th>
								<th scope="col" class="sort" data-sort="status">Kota</th>
								<th scope="col">Aksi</th>
							</tr>
						</thead>
						<tbody class="list">
							@foreach ($group as $key => $brand)
							<tr>
								<td>{{ $brand->id }}</td>
								<td>{{ substr($brand->namagroup, 0, 50) }}</td>
								<td>{{ substr($brand->kota, 0, 50) }}</td>
								<td>
									<div class="form-inline">
										<button type="button" class="btn btn-sm btn-primary" data-target="#modalAdd{{$brand->id}}" data-toggle="modal" onclick="enableUp('{{$brand->flag}}')">Edit</button>
										<form action="{{ route('group-destroy') }}" method="POST">
											@csrf
											<input type="text" hidden value="{{$brand->id}}" id="id" name="id">
											<input type="text" hidden value="{{$brand->flag}}" id="flag" name="flag">
											<button type="submit" class="btn btn-sm btn-danger">Delete</button>
										</form>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- Card footer -->
				<div class="card-footer py-4">
					<nav aria-label="...">
						<ul class="pagination justify-content-end mb-0">
							
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>

	{{-- modal add --}}
	<div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
		<div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
			<div class="modal-content">

				<div class="modal-header">
					<h3 class="modal-title" id="modal-title-default">Tambah Group</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<form method="POST" action="{{ route('group-create') }}">
					@csrf
					<div class="modal-body">
						<div class="mb-3">
							<label for="name" class="form-label">Nama Group</label>
							<input type="text" name="name" class="form-control" id="name" placeholder="Nama">
						</div>
						<div class="mb-3">
							<label for="kota" class="form-label">Kota</label>
							<input type="text" name="kota" class="form-control" id="kota" placeholder="Salatiga">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline-primary" class="close" data-dismiss="modal" aria-label="Close">Batal</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<br />
	{{-- modal edit --}}
	@foreach ($group as $brand)
	<div class="modal fade" id="modalAdd{{ $brand->id }}" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
		<div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
			<div class="modal-content">

				<div class="modal-header">
					<h3 class="modal-title" id="modal-title-default">Update Group</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<form action="{{ route('group-update') }}" method="POST">
					@csrf
					<div class="modal-body">
						<div class="mb-3">
							<input hidden type="text" name="up-id" class="form-control" id="up-id" value="{{$brand->id}}">
						</div>
						<div class="mb-3">
							<label for="up-name" class="form-label">Nama Group</label>
							<input type="text" name="up-name" class="form-control" id="up-name" value="{{$brand->namagroup}}" placeholder="Nama">
						</div>
						<div class="mb-3">
							<label for="up-kota" class="form-label">Kota</label>
							<input type="text" name="up-kota" class="form-control" id="up-kota" value="{{$brand->kota}}">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline-primary" class="close" data-dismiss="modal" aria-label="Close">Batal</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	@endforeach
	@include('layouts.footers.auth')
</div>
@endsection

@push('js')
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
<script>
	// edit brand
	var loadActiveImage = function(event, brandId) {
		var reader = new FileReader();
		reader.onload = function() {
			$('#cloud_icon_active' + brandId).hide();
			var img_prev = document.getElementById('img_preview_active' + brandId);
			img_prev.style = 'width: 100px; height: 100px'
			img_prev.src = reader.result;
		};
		reader.readAsDataURL(event.target.files[0]);
	};
	var loadDisableImage = function(event, brandId) {
		var reader = new FileReader();
		reader.onload = function() {
			$('#cloud_icon_disable' + brandId).hide();
			var img_prev = document.getElementById('img_preview_disable' + brandId);
			img_prev.style = 'width: 100px; height: 100px'
			img_prev.src = reader.result;
		};
		reader.readAsDataURL(event.target.files[0]);
	};


	// add new brand
	$('#modalAddClose').click(function() {
		$('#active_image_icon1').show();
		$('#disable_image_icon1').show();
		$('#active_image_active1').removeAttr("src");
		$('#active_image_active1').removeAttr("style");
		$('#disable_image_disable1').removeAttr("src");
		$('#disable_image_disable1').removeAttr("style");
	})

	var loadActiveImage1 = function(event) {
		var reader = new FileReader();
		reader.onload = function() {
			$('#active_image_icon1').hide();
			var img_prev = document.getElementById('active_image_active1');
			img_prev.style = 'width: 100px; height: 100px'
			img_prev.src = reader.result;
		};
		reader.readAsDataURL(event.target.files[0]);
	};
	var loadDisableImage1 = function(event, ) {
		var reader = new FileReader();
		reader.onload = function() {
			$('#disable_image_icon1').hide();
			var img_prev = document.getElementById('disable_image_disable1');
			img_prev.style = 'width: 100px; height: 100px'
			img_prev.src = reader.result;
		};
		reader.readAsDataURL(event.target.files[0]);
	};

	function handleClickImage(image) {
		let thumbnail_product = $('#image_thumbnail');
		thumbnail_product.attr('src', image);
	}
	$('#sortFilter').on('change', function() {
		let choice = this.value;
		if (!choice) return;
		let url = new URL(window.location.href);
		url.searchParams.set('limit', choice);
		window.location.href = url;
	});

	function enable(element) {
		if (element.value == 2) {
			document.getElementById('desa').style.display = 'block';
			document.getElementById('alamat').style.display = 'block';
		} else {
			document.getElementById('desa').style.display = 'none';
			document.getElementById('alamat').style.display = 'none';
		}
	}

	function enableUp(element) {
		if (element == 2) {
			document.getElementById('u-desa').style.display = 'block';
			document.getElementById('u-alamat').style.display = 'block';
		} else {
			document.getElementById('u-desa').style.display = 'none';
			document.getElementById('u-alamat').style.display = 'none';
		}
	}
</script>
<style>
	#desa {
		display: none;
	}

	#alamat {
		display: none;
	}

	#u-desa {
		display: none;
	}

	#u-alamat {
		display: none;
	}
</style>
@endpush