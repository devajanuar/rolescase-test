<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::post('login', [App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login');

Route::group(['middleware' => 'auth', 'menu'], function () {
	Route::get('/home', [HomeController::class, 'index'])->name('home');
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
	
	//TODO
	Route::post('add-todo', [HomeController::class, 'addTodo']);
	Route::post('edit-todo', [HomeController::class, 'editTodo']);
	Route::post('delete-todo', [HomeController::class, 'destroyTodo']);

	//User
	Route::get('user', [App\Http\Controllers\UserController::class, 'index'])->name('user');
	Route::get('user-search', [App\Http\Controllers\UserController::class, 'search'])->name('user-search');
	Route::post('user-create', [App\Http\Controllers\UserController::class, 'create'])->name('user-create');
	Route::post('user-update', [App\Http\Controllers\UserController::class, 'update'])->name('user-update');
	Route::post('user-destroy', [App\Http\Controllers\UserController::class, 'destroy'])->name('user-destroy');
	Route::post('password-update', [App\Http\Controllers\UserController::class, 'updatePassword'])->name('password-update');

	//Group
	Route::get('group', [App\Http\Controllers\MasterController::class, 'index_group'])->name('group');
	Route::post('group-create', [App\Http\Controllers\MasterController::class, 'add_group'])->name('group-create');
	Route::post('group-update', [App\Http\Controllers\MasterController::class, 'update_group'])->name('group-update');
	Route::post('group-destroy', [App\Http\Controllers\MasterController::class, 'destroy_group'])->name('group-destroy');

	//Member
	Route::get('member', [App\Http\Controllers\MasterController::class, 'index_member'])->name('member');
	Route::post('member-create', [App\Http\Controllers\MasterController::class, 'add_member'])->name('member-create');
	Route::post('member-update', [App\Http\Controllers\MasterController::class, 'update_member'])->name('member-update');
	Route::post('member-destroy', [App\Http\Controllers\MasterController::class, 'destroy_member'])->name('member-destroy');
	Route::post('member-import', [App\Http\Controllers\MasterController::class, 'import_member'])->name('member-import');
});

